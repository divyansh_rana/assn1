package com.example.divyansh.moodle_iitd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button login = (Button) findViewById(R.id.login_button);
        login.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     //proceed to course details from login screen
                                     public void onClick(View view) {
                                         Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                                         startActivity(intent);
                                     }
                                 }
        );



    }
}
