package com.example.divyansh.moodle_iitd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        Button getstarted = (Button) findViewById(R.id.launch_proceed_button);
        getstarted.setOnClickListener(new View.OnClickListener() {
                                          @Override                                         //proceed to login page from the launch activity
                                          public void onClick(View view) {
                                              Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                              startActivity(intent);
                                          }
                                      }
        );


    }


}
